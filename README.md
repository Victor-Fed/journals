# Тестовое задание "Создание справочника"

## Системные требования
Для запуска приложения требуется:

-   `docker >= 18.0` _(install: `curl -fsSL get.docker.com | sudo sh`)_
-   `docker-compose >= 3` _([installing manual](https://docs.docker.com/compose/install/#install-compose))_
-   `git`
- `composer`



### Запуск приложения

Выполните следующие команды:

```
$ git clone https://Victor-Fed@bitbucket.org/Victor-Fed/journals.git
$ cd journals
$ docker-compose up -d
$ docker-compose  exec  frontend composer install
$ docker-compose  exec  frontend php ./init  
```
Далее укажите в файле common\config\main-local.php конфиги для db(указаны в docker-compose.yml), пример:
```
'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql;dbname=yii2advanced',
            'username' => 'yii2advanced',
            'password' => 'secret',
            'charset' => 'utf8',
        ],
)
```
Далее выполните следующие команды:
```
$ docker-compose  exec  frontend php ./yii migrate 
$ docker-compose  exec  frontend php ./yii seed
```
<strong>Готово!</strong>
Приложение будет доступно по [localhost:20080](http://localhost:20080)
