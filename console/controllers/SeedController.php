<?php
namespace console\controllers;

use frontend\models\Author;
use frontend\models\Journal;
use yii\console\Controller;
use yii\db\Exception;

class SeedController extends Controller
{
    public $authorsList = [
        [
            'id'=>1,
            'name'=>'Александр',
            'family'=>'Пушкин',
            'patronymic'=>'Сергеевич'
        ],
        [
            'id'=>2,
            'name'=>'Владимир   ',
            'family'=>'Маяковский',
            'patronymic'=>'Владимирович'
        ],
        [
            'id'=>3,
            'name'=>'Николай',
            'family'=>'Гоголь',
            'patronymic'=>'Васильевич'
        ]
    ];
    public $jourlanList = [
            [
                'title'=> 'Наука и жизнь',
                'description'=> 'Описание Наука и жизнь',
                'authors'=>[1,2]
            ],
            [
                'title'=> 'Космос',
                'description'=> 'Описание Космос',
                'authors'=>[3]
            ],
    ];

    /**
     * @return bool
     */
    public function actionIndex()
    {
        try {
            $this->addAuthors();
            $this->addJournals();
        } catch (Exception $e) {
            echo $e->getMessage().PHP_EOL;
            return false;
        }

        echo 'Тестовые данные успешно добавлены'.PHP_EOL;
        return true;
    }

    /**
     * @throws Exception
     */
    protected function addAuthors()
    {
        foreach ($this->authorsList as $authorData) {
            $author = new Author();
            $author->id = $authorData['id'];
            $author->name = $authorData['name'];
            $author->family = $authorData['family'];
            $author->patronymic = $authorData['patronymic'];
            if (!$author->save()) {
                throw new Exception('Ошибки при сохранении'.serialize($author->errors));
            }
        }
    }

    /**
     * @throws Exception
     */
    protected function addJournals()
    {
        foreach ($this->jourlanList as $jourlanData) {
            $jourlan = new Journal();
            $jourlan->title = $jourlanData['title'];
            $jourlan->description = $jourlanData['description'];
            $jourlan->authorsArray = $jourlanData['authors'];
            if (!$jourlan->save(false)) {
                throw new Exception('Ошибки при сохранении'.serialize($jourlan->errors));
            }
            $jourlan->saveAuthors();
        }
    }
}
