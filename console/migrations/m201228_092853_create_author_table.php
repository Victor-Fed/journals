<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%author}}`.
 */
class m201228_092853_create_author_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%author}}', [
            'id' => $this->primaryKey(),
            'family' => $this->string(255)->notNull()->comment('Имя'),
            'name' => $this->string(255)->notNull()->comment('Фамилия'),
            'patronymic' => $this->string(255)->comment('Отчество'),
        ],$tableOptions);

        $this->createIndex('idx_author_family', '{{%author}}', 'family');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%author}}');
    }
}
