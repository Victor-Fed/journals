<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%author_journal}}`.
 */
class m201228_093651_create_author_journal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%author_journal}}', [
            'id' => $this->primaryKey(),
            'id_author' => $this->integer(),
            'id_journal' => $this->integer(),
        ]);

        $this->createIndex('idx_author_journal_id_author', '{{%author_journal}}', 'id_author');
        $this->createIndex('idx_author_journal_id_journal', '{{%author_journal}}', 'id_journal');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%author_journal}}');
    }
}
