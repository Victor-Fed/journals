<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%image}}`.
 */
class m201228_093914_create_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(),
            'hash' => $this->string(36)->unique()->comment('Хэш файла'),
        ]);
        $this->createIndex('idx_image_hash', '{{%image}}', 'hash');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%image}}');
    }
}
