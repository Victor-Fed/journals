<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%journal_image}}`.
 */
class m201228_093925_create_journal_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%journal_image}}', [
            'id' => $this->primaryKey(),
            'id_journal' => $this->integer(),
            'id_image' => $this->integer()

            ,
        ]);
        $this->createIndex('idx_journal_image_id_journal', '{{%journal_image}}', 'id_journal');
        $this->createIndex('idx_journal_image_id_image', '{{%journal_image}}', 'id_image');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%journal_image}}');
    }
}
