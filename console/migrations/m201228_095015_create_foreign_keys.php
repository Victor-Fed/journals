<?php

use yii\db\Migration;

/**
 * Class m201228_095015_create_foreign_keys
 */
class m201228_095015_create_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk-journal-author-journal', 'author_journal', 'id_journal', 'journal', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk-author-author-journal', 'author_journal', 'id_author', 'author', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk-journal-journal-image', 'journal_image', 'id_journal', 'journal', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk-image-journal-image', 'journal_image', 'id_image', 'image', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-journal-author-journal', 'author_journal');
        $this->dropForeignKey('fk-author-author-journal', 'author_journal');
        $this->dropForeignKey('fk-journal-journal-image', 'journal_image');
        $this->dropForeignKey('fk-image-journal-image', 'journal_image');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201228_095015_create_foreign_keys cannot be reverted.\n";

        return false;
    }
    */
}
