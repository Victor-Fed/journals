<?php


namespace frontend\controllers;

use frontend\models\Image;
use Yii;
use yii\debug\models\timeline\Search;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ImageController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'get' => ['get'],
                ],
            ],
        ];
    }

    public function actionGet($hash)
    {
        $model = $this->findModel($hash);
        $path  = Image::uploadDir().$hash;
        $files = scandir($path);
        return Yii::$app->response->sendFile($path.'/'.$files[2]);
    }


    /**
     * Finds the Journal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Image the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($hash)
    {
        if (($model = Image::findOne(['hash'=>$hash])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Изображение не найдено');
    }
}
