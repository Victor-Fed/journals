<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "author_journal".
 *
 * @property int $id
 * @property int|null $id_author
 * @property int|null $id_journal
 *
 * @property Author $author
 * @property Journal $journal
 */
class AuthorJournal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'author_journal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_author', 'id_journal'], 'integer'],
            [['id_author'], 'exist', 'skipOnError' => true, 'targetClass' => Author::class, 'targetAttribute' => ['id_author' => 'id']],
            [['id_journal'], 'exist', 'skipOnError' => true, 'targetClass' => Journal::class, 'targetAttribute' => ['id_journal' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_author' => 'Id Author',
            'id_journal' => 'Id Journal',
        ];
    }

    /**
     * Gets query for [[Author]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::class, ['id' => 'id_author']);
    }

    /**
     * Gets query for [[Journal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJournal()
    {
        return $this->hasOne(Journal::class, ['id' => 'id_journal']);
    }
}
