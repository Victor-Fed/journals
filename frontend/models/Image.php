<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property string|null $hash Хэш файла
 *
 * @property JournalImage[] $journalImages
 */
class Image extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hash'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hash' => 'Hash',
        ];
    }

    public static function uploadDir()
    {
        return  Yii::getAlias('@app').'/../uploads/';
    }

    /**
     * Gets query for [[JournalImages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJournalImages()
    {
        return $this->hasMany(JournalImage::class, ['id_image' => 'id']);
    }

    public static function getHash($path)
    {
        return md5_file($path);
    }
}
