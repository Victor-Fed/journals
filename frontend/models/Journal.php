<?php

namespace frontend\models;

use Faker\Provider\Uuid;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "journal".
 *
 * @property int $id
 * @property string $title Название журнала
 * @property string|null $description Короткое описание
 * @property string $date_create
 *
 * @property AuthorJournal[] $authorJournals
 * @property JournalImage[] $journalImages
 */
class Journal extends \yii\db\ActiveRecord
{
    public $imageFile;
    public $authorsArray;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'journal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['date_create'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions' => 'png, jpg','maxSize' => 1024 * 1024 * 2],
            [['authorsArray'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
            'date_create' => 'Время выпуска журнала',
            'imageFile' => 'Картинка',
            'authorsArray' => 'Список авторов',
        ];
    }


    public function getAuthors()
    {
        return $this->hasMany(Author::class, ['id' => 'id_author'])->viaTable(AuthorJournal::tableName(), ['id_journal' => 'id']);
    }
    /**
     * Gets query for [[JournalImages]].
     *
     * @return \yii\db\ActiveQuery
     */
    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getImage()
    {
        return $this->hasMany(Image::class, ['id' => 'id_image'])->viaTable(JournalImage::tableName(), ['id_journal' => 'id'])->one();
    }

    /**
     * Gets query for [[JournalImages]].
     *
     * @return \yii\db\ActiveQuery
     */
    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getImages()
    {
        return $this->hasMany(Image::class, ['id' => 'id_image'])->viaTable(JournalImage::tableName(), ['id_journal' => 'id']);
    }

    public function saveAuthors()
    {
        if ($this->authorsArray) {
            foreach ($this->authorsArray as $id) {
                if (($author = Author::findOne($id)) !== null) {
                    $this->link('authors', $author);
                }
            }
        }
    }

    /**
     * @return array|bool|Image|\yii\db\ActiveRecord|null
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        if ($this->validate()) {
            $imageFile = UploadedFile::getInstance($this, 'imageFile');
            if (empty($imageFile)) {
                return false;
            }
            $hash = Image::getHash($imageFile->tempName);
            $uploadDir = Image::uploadDir();

            $path = $uploadDir.$hash.'/';
            if (FileHelper::createDirectory($path)) {
                if ($findOne = Image::find()->where(['hash'=>$hash])->one()) {
                    return $findOne;
                }
                $imageFile->saveAs($path . $imageFile->baseName . '.' . $imageFile->extension);
                $image = new Image();
                $image->hash = $hash;
                $image->save();
                return $image;
            }

            return false;
        } else {
            return false;
        }
    }

    public function getSelectedAuthor()
    {
        $list =  [];
        foreach ($this->authors as $author) {
            $list[$author->id] = ['selected' => true];
        }
        return $list;
    }

    public function removeAuthors()
    {
        foreach ($this->authors as $author) {
            $this->unlink('authors', $author);
        }
    }
}
