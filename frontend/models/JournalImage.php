<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "journal_image".
 *
 * @property int $id
 * @property int|null $id_journal
 * @property int|null $id_image
 *
 * @property Image $image
 * @property Journal $journal
 */
class JournalImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'journal_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_journal', 'id_image'], 'integer'],
            [['id_image'], 'exist', 'skipOnError' => true, 'targetClass' => Image::class, 'targetAttribute' => ['id_image' => 'id']],
            [['id_journal'], 'exist', 'skipOnError' => true, 'targetClass' => Journal::class, 'targetAttribute' => ['id_journal' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_journal' => 'Id Journal',
            'id_image' => 'Id Image',
        ];
    }

    /**
     * Gets query for [[Image]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::class, ['id' => 'id_image']);
    }

    /**
     * Gets query for [[Journal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJournal()
    {
        return $this->hasOne(Journal::class, ['id' => 'id_journal']);
    }
}
