<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Авторы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать автора', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'family',
            'name',
            'patronymic',
            [
                'label'=>'Журналы',
                'format' => 'raw',
                'value' => function ($dataProvider) {
                    if ($dataProvider->journals) {
                        $links = [];
                        foreach ($dataProvider->journals as $journals) {
                            $links[] = ['title'=>$journals->title, 'id'=>$journals->id];
                        }

                        return Html::ul($links, ['class' => 'list-group',
                            'item' => function ($item, $index) {
                                return Html::tag('li', Html::a($item['title'], Url::base(true).'/journal/view?id='.$item['id']));
                            }]);
                    }
                    return 'Не указаны';
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
