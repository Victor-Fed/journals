<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Author */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Авторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="author-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'delete',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'family',
            'name',
            'patronymic',
            [
                'attribute'=>'journals',
                'label'=>'Журналы',
                'format' => 'raw',
                'value'=> function ($model) {
                    if ($model->journals) {
                        $links = [];
                        foreach ($model->journals as $journal) {
                            $links[] = ['title'=>$journal->title , 'id'=>$journal->id];
                        }

                        return Html::ul($links, ['class' => 'list-group',
                            'item' => function ($item, $index) {
                                return Html::tag('li', Html::a($item['title'], Url::base(true).'/journal/view?id='.$item['id']));
                            }]);
                    }
                    return 'Не указаны';
                }
            ]
        ],
    ]) ?>

</div>
