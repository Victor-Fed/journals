<?php

use frontend\models\Author;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Journal */
/* @var $form yii\widgets\ActiveForm */
/* @var $authors frontend\models\Author */
?>

<div class="journal-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    if (isset($image)) {
        $hash = $model->image->hash ?? null;
        $path = ($hash) ? Url::base(true).'/image/get?hash=' . $hash : Url::base(true).'/img/empty.jpg';
        echo Html::img($path,['width'=>'100','height'=>'100']);
    }
    ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date_create')->textInput() ?>
    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'authorsArray[]')->dropDownList(ArrayHelper::map($authors, 'id', 'name'),
        [
            'multiple'=>'multiple',
            'options' => $model->getSelectedAuthor()

        ]
    )->label("Автор(ы)");  ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
