<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Journal */
/* @var $authors frontend\models\Author */
$this->title = 'Создание журнала';
$this->params['breadcrumbs'][] = ['label' => 'Журналы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'authors' => $authors,
    ]) ?>

</div>
