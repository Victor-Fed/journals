<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Журналы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать журнал', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'date_create',


            [
            'label'=>'Картинка',
            'format' => ['image',['width'=>'50','height'=>'50']],
            'value' => function ($dataProvider) {
                $hash = $dataProvider->image->hash ?? null;
                $path = ($hash) ? Url::base(true).'/image/get?hash=' . $hash : Url::base(true).'/img/empty.jpg';
                return $path;
            },
             ],

            [
                'label'=>'Авторы',
                'format' => 'raw',
                'value' => function ($dataProvider) {
                    if ($dataProvider->authors) {
                        $links = [];
                        foreach ($dataProvider->authors as $author) {
                            $links[] = ['name'=>$author->name . ' ' . $author->family . ' ' . $author->patronymic, 'id'=>$author->id];
                        }

                        return Html::ul($links, ['class' => 'list-group',

                                         'item' => function ($item, $index) {
                                             return Html::tag('li', Html::a($item['name'], Url::base(true).'/author/view?id='.$item['id']));
                                         }]);
                    }
                    return 'Не указаны';
                },
            ],

            [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'delete'=>function ($url, $model, $key) {
                            return Html::a("", $url, [
                                'class' => 'glyphicon glyphicon-trash',
                                'title'=>"Удалить",
                                'data' => [
                                    'method' => 'delete',
                                    'pjax'=>0,
                                    'confirm'=>"Вы уверены, что хотите удалить этот элемент?",

                                ],
                            ]);
                        }
                    ]
            ],

        ],
    ]); ?>


</div>
