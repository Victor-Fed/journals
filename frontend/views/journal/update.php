<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Journal */
/* @var $authors frontend\models\Author */

$this->title = 'Редактирование журнала: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Журналы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="journal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'authors' => $authors,
        'image' => $model->image
    ]) ?>

</div>
