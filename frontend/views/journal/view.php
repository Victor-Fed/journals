<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Journal */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Журналы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$hash = $model->image->hash ?? null;
$path = ($hash) ? Url::base(true).'/image/get?hash=' . $hash : Url::base(true).'/img/empty.jpg';
?>
<div class="journal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'delete',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description:ntext',
            'date_create',
            [
                'attribute'=>'Картинка',
                'value'=>($path),
                'format' => ['image',['width'=>'100','height'=>'100']]
            ],
            [
                    'attribute'=>'Авторы',
                    'format' => 'raw',
                    'value'=> function ($model) {
                        if ($model->authors) {
                            $links = [];
                            foreach ($model->authors as $author) {
                                $links[] = ['name'=>$author->name . ' ' . $author->family . ' ' . $author->patronymic, 'id'=>$author->id];
                            }

                            return Html::ul($links, ['class' => 'list-group',

                                         'item' => function ($item, $index) {
                                             $class = $index == 0 ? 'first-item' : 'item';

                                             return Html::tag('li', Html::a($item['name'], Url::base(true).'/author/view?id='.$item['id']), ['class' => $class]);
                                         }]);
                        }
                        return 'Не указаны';
                    }
            ]
        ],
    ]) ?>

</div>
